/**
 * O método unshift() adiciona um ou mais elementos ao início de um array 
 * e retorna o novo comprimento do array.
*/

const array1 = [1, 2, 3]

const result = array1.unshift(4, 5)

console.log(result)

console.log(array1)

