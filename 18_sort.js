/**
 * O método sort() classifica os elementos de um array no lugar 
 * e retorna a referência ao mesmo array, agora classificado. 
 * A ordem de classificação padrão é crescente, 
 * construída na conversão dos elementos em strings e, em seguida, 
 * comparando suas sequências de valores de unidades de código UTF-16.
*/

/**
 * A complexidade de tempo e espaço do tipo não pode ser garantida, 
 * pois depende da implementação.
*/

const products = require('./@products')

const result = products
  .map((product) => product.price)
  .sort()

console.log(result)
