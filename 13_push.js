/** 
 * O método push() adiciona um ou mais elementos ao final de um array 
 * e retorna o novo comprimento do array. 
 * Este método altera o comprimento do array.
*/

const animals = ['pigs', 'goats', 'sheep']

const result = animals.push('cows')
console.log(result)

console.log(animals)