/**
 * O método forEach() executa uma função fornecida uma vez para cada elemento do array.
 * Recebe uma função de callback
*/ 

const array1 = ['a', 'b', 'c']
const array2 = [4, 6, 8, 12]

array1.forEach(element => console.log(element))

function logArrayElements(element, index, array) {
  console.log("a[" + index + "] = " + element);
}

array2.forEach(logArrayElements);
