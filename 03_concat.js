/** 
 * O método concat() é usado para mesclar dois ou mais arrays. 
 * Este método não altera os arrays existentes, mas retorna um novo array.
*/

const array1 = ['a', 'b', 'c'];
const array2 = ['d', 'e', 'f']

const array3 = array1.concat(array2)
console.log(array3)

const numbers1 = [1, 2, 3]
const numbers2 = [4, 5, 6]

const numbers3 = numbers1.concat(numbers2)
console.log(numbers3)