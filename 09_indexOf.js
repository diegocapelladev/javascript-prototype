/**
 * O método indexOf() retorna o primeiro índice no qual
 *  um determinado elemento pode ser encontrado no array, 
 * ou -1 se não estiver presente.
*/

const beasts = ['ant', 'bison', 'camel', 'duck', 'bison']

const result = beasts.indexOf('bison')
console.log(result)

// Começando a partir do índice passado
console.log(beasts.indexOf('bison', 2))

console.log(beasts.indexOf('giraffe'))