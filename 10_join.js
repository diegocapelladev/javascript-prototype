/**
 * O método join() cria e retorna uma nova string concatenando todos os elementos em um array
 * (ou um objeto semelhante a um array), 
 * separados por vírgulas ou uma string separadora especificada. 
 * Se o array tiver apenas um item, esse item será retornado sem usar o separador.
*/

const elements = ['Fire', 'Air', 'Water']

const result = elements.join()
console.log(result)

console.log(elements.join(''))

console.log(elements.join('-'))
