/**
 * O método some() testa se pelo menos um elemento no array passa no teste 
 * implementado pela função fornecida. Retorna verdadeiro se, no array, 
 * encontrar um elemento para o qual a função fornecida retorna verdadeiro; 
 * caso contrário, retorna falso. 
 * Não modifica o array.
*/

const products = require('./@products')

const result = products.some((product) => product.id === '6228ff31b7e6cb904bbe0172')
console.log(result)