/** 
 * O método estático Array.from() cria uma nova instância de Array 
 * copiada superficialmente a partir de um objeto iterável 
 * ou semelhante a um array.
*/

const foo = 'foo'
console.log(Array.from(foo))

const numbers  = [1, 2, 3]
console.log(Array.from(numbers, x => x + x))