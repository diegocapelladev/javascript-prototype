/**
 * O método includes() determina se um array inclui um determinado valor entre suas entradas, 
 * retornando true ou false conforme apropriado.
 * Typos permitidos na verificação: Boolean, Null, Undefined, Number, String e Symbol (ES2015)
*/

const array1 = [1, 2, 3]
const array2 = ['cat', 'dog', 'bat']

const result = array2.includes('cat')
console.log(result)

const products = require('./@products')

const product = products.filter(prod => prod.name.includes('Jaqueta'))
console.log(product)

// Não funciona com objetos!
const searchProduct = {
  name: "Vestido Vermelho",
  price: 250,
  id: "6228ff93b7e6cb904bbe0177",
}

// Vai retornar sempre false
const resultSearch = product.includes(searchProduct)
console.log(resultSearch)