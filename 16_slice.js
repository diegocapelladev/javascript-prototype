/**
 * O método slice() retorna uma cópia rasa de uma parte de um array 
 * em um novo objeto array selecionado do início ao fim (final não incluído) 
 * onde start e end representam o índice de itens naquele array. 
 * O array original não será modificada.
*/

const animals = ['ant', 'bison', 'camel', 'duck', 'elephant']

// Retorna os elementos a partir do índice passado
// console.log(animals.slice(2))

// Retorna os elementos do índice inicial até o índice final(não inclui)
// console.log(animals.slice(0, 3))

// Retorna os elementos do índice inicial, excluindo o índice final
console.log(animals.slice(2, -1))