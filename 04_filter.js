/**
 * O método filter() cria uma cópia superficial de uma parte de um determinado array, 
 * filtrada apenas para os elementos do array fornecido 
 * que passam no teste implementado pela função fornecida. 
 * Recebe uma função de callback
*/ 
const products = require('./@products')

const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present']

const result = words.filter(word => word.length > 6)
// console.log(result)

const getNumbers = (value) => {
  return value >= 5 && value % 2 === 0
}

const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
// const filtered = numbers.filter((value) => value >= 5 && value % 2 === 0 )
const filtered = numbers.filter(getNumbers)
console.log(filtered)

console.log(products.length)

// const productId = "6228fe80b7e6cb904bbe0168"

// const productsResult = products.filter((product) => product.id !== productId)
const productsResult = products.filter((product) => product.price <= 200)
console.log(productsResult.length)
console.log(productsResult)