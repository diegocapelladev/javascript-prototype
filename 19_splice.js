/**
 * O método splice() altera o conteúdo de um array removendo ou substituindo 
 * elementos existentes e/ou adicionando novos elementos no lugar. 
 * Para acessar parte de um array sem modificá-lo, veja slice().
*/

const months = ['Jan', 'March', 'April', 'June']

// Adiciona um elemento no índice (1), 
// quantidade de elementos a serem removidos(0) 
// e os elementos a serem adicionados ('Feb')
months.splice(1, 0, 'Feb')
console.log(months)

// months.splice(4, 0, 'May')
months.splice(4, 1, 'May')
console.log(months)
