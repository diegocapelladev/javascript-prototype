/**
 * O método toString() retorna uma string representando o array especificado 
 * e seus elementos.
*/

const array1 = [1, 2, 'a', '1a']
console.log(array1.toString())