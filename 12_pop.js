/**
 * O método pop() remove o último elemento de um array e retorna esse elemento. 
 * Este método altera o comprimento do array.
*/

const plants = ['broccoli', 'cauliflower', 'cabbage', 'kale', 'tomato']

const result = plants.pop()
console.log(result)

console.log(plants)