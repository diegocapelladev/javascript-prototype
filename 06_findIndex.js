/**
 * O método findIndex() retorna o índice do primeiro elemento em um array 
 * que satisfaça a função de teste fornecida. 
 * Se nenhum elemento satisfizer a função de teste, -1 será retornado
 * Recebe uma função de callback
*/ 

const numbers = [4, 6, 8, 12]

const beasts = ['ant', 'bison', 'camel', 'duck', 'bison']

const checkNumber = (element, index, array) => {
  // return element === 8
  // return index === 1
  return array
}
// const result = numbers.findIndex(checkNumber)

// const result = numbers.findIndex((element) => element === 14)
// const result = numbers.findIndex((element) => element === 6)
const result = beasts.findIndex((element) => element === 'ant')

console.log(result)