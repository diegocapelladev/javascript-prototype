const products = [
  {
    id: "6228fe63b7e6cb904bbe0165",
    price: 150,
    name: "Suéter Branco",
  },
  {
    id: "6228fec7b7e6cb904bbe016f",
    name: "Regata Azul",
    price: 150,
  },
  {
    price: 200,
    id: "6228ff71b7e6cb904bbe0175",
    name: "Suéter Vermelho Vibrante",
  },
  {
    name: "Vestido Vermelho",
    price: 250,
    id: "6228ff93b7e6cb904bbe0177",
  },
  {
    price: 300,
    id: "6228fe80b7e6cb904bbe0168",
    name: "Jaqueta Xadrez",
  },
  {
    price: 200,
    name: "Blusa Branca",
    id: "6228ff31b7e6cb904bbe0172",
  },
  {
    price: 200,
    id: "62290014b7e6cb904bbe017c",
    name: "Calça Jeans Azul",
  },
  {
    id: "6228ffa5b7e6cb904bbe017a",
    name: "Vestido Branco",
    price: 250,
  },
]

module.exports = products