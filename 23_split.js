/**
 * O método split() pega um padrão e divide uma String em uma lista ordenada de substrings
 *  procurando pelo padrão, coloca essas substrings em um array e retorna o array.
*/

const str = 'The quick brown fox jumps over the lazy dog.'

const words = str.split(' ')
console.log(words[3])

const chars = str.split('')
console.log(chars[8])

const strCopy = str.split()
console.log(strCopy)

const monthString = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec"

const monthResult = monthString.split(',')
console.log(monthResult)

const accessToken = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'

// const token = accessToken.split(' ')
// console.log(token)
// console.log(token[1])

const [, token] = accessToken.split(' ')
console.log(token)