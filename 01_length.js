/**
 * A propriedade length de um objeto Array representa o número de elementos nesse array. 
 * O valor é um inteiro sem sinal de 32 bits 
 * que é sempre numericamente maior que o índice mais alto no array. 
*/ 

const clothing = ['shoes', 'shirts', 'socks', 'sweaters']
const abc = ["a", "b", "c", "d", "e", "f"]

console.log(clothing.length)
console.log(abc.length)