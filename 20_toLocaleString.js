/**
 * O método toLocaleString() retorna uma string representando os elementos do array. 
 * Os elementos são convertidos em Strings usando seus métodos toLocaleString 
 * e esses Strings são separados por uma String específica do local (como uma vírgula “,”).
*/

const array1 = [1, 'a', new Date('21 Dec 1997 14:12:00 UTC')]

// const localeString = array1.toLocaleString('en', { timeZone: 'UTC' })
const localeString = array1.toLocaleString('pt-BR', { timeZone: 'UTC' })

console.log(localeString)