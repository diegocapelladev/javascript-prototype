/**
 * O método map() cria um nova array preenchida com os resultados 
 * da chamada de uma função fornecida em cada elemento do array de chamada.
*/

const array1 = [1, 4, 9, 16]

const map1 = array1.map(x => x * 2)
console.log(map1)

const products = require('./@products')

const result = products.map((product) => product.name)
console.log(result)

const str = result.join()
console.log(str)