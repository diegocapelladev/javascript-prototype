/**
 * O método shift() remove o primeiro elemento de um array e retorna esse elemento removido. 
 * Este método altera o comprimento do array.
*/

const products = require('./@products')

const animals = ['ant', 'bison', 'camel', 'duck', 'elephant']

const firstElement = animals.shift()
console.log(firstElement)

console.log(animals)

const result = products.shift()
console.log(result.id)