/**
 * O método find() retorna o primeiro elemento do array fornecido 
 * que satisfaz a função de teste fornecida. 
 * Se nenhum valor satisfizer a função de teste, undefined será retornado.
 * Recebe uma função de callback
*/ 

const products = require('./@products')

// const result = products.find((product) => product.id === '6228fec7b7e6cb904bbe016f')
// const result = products.find((product) => product.price > 1050)
const result = products.find((product) => product.price >= 150)
console.log(result)