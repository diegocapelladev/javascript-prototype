/**
 * O método reduce() executa uma função de retorno de chamada “reducer” 
 * fornecida pelo usuário em cada elemento do array, em ordem, 
 * passando o valor de retorno do cálculo no elemento anterior. 
 * O resultado final da execução do reducer em todos os elementos do array é um único valor.
*/

/**
 * Na primeira vez que o callback é executado, não há “valor de retorno do cálculo anterior”. 
 * Se fornecido, um valor inicial pode ser usado em seu lugar. 
 * Caso contrário, o elemento do array no índice 0 é usado como o valor inicial 
 * e a iteração começa no próximo elemento (índice 1 em vez do índice 0).
*/

/**
 * Talvez o caso mais fácil de entender reduce() 
 * seja retornar a soma de todos os elementos em um array
*/

const products = require('./@products')

function myReduce(array, callback, initialValue) {
  let accumulator = initialValue
  for (let i = 0; i < array.length; i++) {
    accumulator = callback(
      accumulator,
      array[i],
      i,
      array
    )
  }
  return accumulator
}

const prices = products
  .map((product) => product.price)
  // .reduce((acc, curr) => acc + curr, 0)
console.log(prices)

const total = prices.reduce((acc, curr) => acc + curr, 0)
console.log(total)

const res = myReduce(prices, (acc, curr) =>  acc + curr, 0)
console.log(res)